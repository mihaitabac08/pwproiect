import React, {Component} from 'react';
import axios from 'axios';
import {Button, Card, Form} from "react-bootstrap";
import {toast} from 'react-toastify';
import { Redirect } from "react-router-dom"

class RegisterSupport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            username: '',
            password: ''
        }
    };

    registerHandler = (event) => {
        event.preventDefault();
        axios.post("http://localhost:3001/api/v1/Users/registerSupport", {
            username: this.state.username,
            password: this.state.password
        }).then(response => {
            toast("Utilizatorul a fost creat!");
            this.setState({
                firstName: '',
                lastName: '',
                email: '',
                username: '',
                password: ''
            });
            console.log(response)

            this.setState({ redirect: "/authenticate" });
        })
            .catch(error => {
               //    toast(error.response.data.error);
                console.error(error)
            });
    };


    changeHandler = (event) => {
        event.preventDefault();
        const inputValue = event.target.value;
        const stateField = event.target.name;
        this.setState({
            [stateField]: inputValue
        });
        console.log(this.state);
    };

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }
        return (
            // Your Code goes here
            <div>
                <Card style={{
                    "padding": "10px",
                    "backgroundColor": "#f9f9f9"
                }}>
                    <Form className="AuthenticateForm" onSubmit={this.registerHandler}>
                        <Form.Group controlId="username">
                            <Form.Label>Username:</Form.Label>
                            <Form.Control type="text"
                                          value={this.state.username}
                                          name="username"
                                          onChange={this.changeHandler}/>

                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>Password:</Form.Label>
                            <Form.Control type="password"
                                          value={this.state.password}
                                          name="password"
                                          onChange={this.changeHandler}/>

                        </Form.Group>

                        <Button variant="primary" type="submit">
                            Register
                        </Button>
                    </Form>
                </Card>
            </div>
        )
    }
}

export default RegisterSupport;
