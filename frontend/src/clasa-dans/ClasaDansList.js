import React from 'react';
import axios from 'axios';
import {Button, Form, Table} from "react-bootstrap";

const baseUrl = 'http://localhost:3001/api/v1';

class ClasaDansList extends React.Component {
    state = {
        authors: [],
        className: ""
    };

    componentDidMount() {
        if (localStorage.getItem('token') != null)
            this.loadClasaDans();
    }

    render() {
        return (
            <div className="AuthorList" style={
                {
                    "padding": "25px",
                    "marginLeft": "25px",
                    "marginRight": "25px"
                }
            }>


                <Form onSubmit={this.submitHandler}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>First name</Form.Label>
                        <Form.Control type="text"
                                      placeholder="Nume clasa"
                                      value={this.state.className}
                                      name="className"
                                      onChange={this.changeHandler}/>
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>

                {
                    this.state.authors !== [] ?
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nume clasa</th>

                            </tr>
                            </thead>
                            <tbody>
                            {this.state.authors.map((author, index) =>
                                <tr key={index}>
                                    <td>{author._id}</td>
                                    <td>{author.className}</td>
                                </tr>
                            )}
                            </tbody>
                        </Table>
                        : null
                }
            </div>
        )
    }

    loadClasaDans = () => {
        console.log(localStorage.getItem('token'));
        axios.get(baseUrl + '/clasaDans', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => this.setState({"authors": response.data, className: ""}))
            .catch(error => console.error(error));
    };

    submitHandler = (event) => {
        event.preventDefault();

        const config = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        };

        const body = {
            className: this.state.className
        };

        axios.post(baseUrl + '/clasaDans',
            body,
            config)
            .then(this.loadClasaDans())
            .catch(error => console.error(error));
    };

    changeHandler = (event) => {
        event.preventDefault();
        const inputValue = event.target.value;
        const stateField = event.target.name;
        this.setState({
            [stateField]: inputValue
        });
    };

}

export default ClasaDansList;
