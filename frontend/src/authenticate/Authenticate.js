import React, {Component} from 'react';
import axios from 'axios';
import {toast} from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import {Button, Card, Form} from "react-bootstrap";
import {Redirect} from "react-router-dom";

class Authenticate extends Component {


    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        }
    };

    submitHandler = (event) => {
        event.preventDefault();
        axios.post("http://localhost:3001/api/v1/Users/login", {
            "username": this.state.username,
            "password": this.state.password
        }).then(response => {
            localStorage.setItem("token", response.data)
            this.setState({ redirect: "/" });
            window.location.reload(false);
        })
            .catch(error => {
                //toast(error.response.data.error);
                console.error(error)
            });
    };

    changeHandler = (event) => {
        event.preventDefault();
        const inputValue = event.target.value;
        const stateField = event.target.name;
        this.setState({
            [stateField]: inputValue
        });
    };

    render() {

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }
        return (

            <div>
                <Card style={{
                    "padding": "10px",
                    "backgroundColor": "#f9f9f9"
                }}>
                    <Form className="AuthenticateForm" onSubmit={this.submitHandler}>
                        <Form.Group controlId="username">
                            <Form.Label>Username:</Form.Label>
                            <Form.Control type="text"
                                          value={this.state.username}
                                          name="username"
                                          onChange={this.changeHandler}/>

                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>Password:</Form.Label>
                            <Form.Control type="password"
                                          value={this.state.password}
                                          name="password"
                                          onChange={this.changeHandler}/>

                        </Form.Group>

                        <Button variant="primary" type="submit">
                            Login
                        </Button>
                    </Form>
                </Card>
            </div>
        )
    }
}

export default Authenticate;
