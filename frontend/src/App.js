import React, {Component} from 'react';
import './App.scss';
import Authenticate from "./authenticate/Authenticate";
import {HashRouter, Link, Route, Switch} from "react-router-dom";
import {Col, Container, Row} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import ClasaDansList from "./clasa-dans/ClasaDansList";
import ProgramareClase from "./programare-clasa/ProgramareClase";
import InscrieriClase from "./inscrieri/InscrieriClase";
import MyCalendar from "./calendar/calendar";
import Register from "./register/Register";
import Mesaj from "./mesaj/mesaj";
import RegisterSupport from "./register-support/RegisterSupport";
import {ToastContainer} from 'react-toastify';

class App extends Component {

    state = {
        auth: false,
    };

    constructor(props, context) {
        super(props, context);
        this.clearState = this.clearState.bind(this);
    }

    componentDidMount() {
        this.setState({
            auth: localStorage.getItem('token') != null
        });
    }

    clearState() {
        localStorage.clear();
        this.setState({
            auth: false
        });
        window.location.reload(false);
    };

    render() {
        return (
            <div className="App">
                <ToastContainer/>
                <HashRouter baseline="/">
                    <Container>
                        <Row>
                            {
                                !this.state.auth ?
                                    <Col>
                                        <Link to="/authenticate">Authenticate</Link>
                                    </Col> : null
                            }
                            {
                                this.state.auth ?
                                    <Col>
                                        <div onClick={this.clearState}>Log out</div>
                                    </Col> : null
                            }
                            {
                                !this.state.auth ?
                                    <Col>
                                        <Link to="/register">Register</Link>
                                    </Col> : null
                            }
                            {
                                !this.state.auth ?
                                    <Col>
                                        <Link to="/registerSupport">Register support</Link>
                                    </Col> : null
                            }
                            {
                                this.state.auth ?
                                    <Col>
                                        <Link to="/className">Nume clasa</Link>
                                    </Col> : null
                            }
                            {
                                this.state.auth ?
                                    <Col>
                                        <Link to="/programareClasa">ProgramClasa</Link>
                                    </Col> : null
                            }
                            {
                                this.state.auth ?
                                    <Col>
                                        <Link to="/inscrieri">Inscrieri</Link>
                                    </Col> : null
                            }
                            {
                                this.state.auth ?
                                    <Col>
                                        <Link to="/mesaj">Mesaj</Link>
                                    </Col> : null
                            }
                            {
                                this.state.auth ?
                                    <Col>
                                        <Link to="/calendar">Calendar</Link>
                                    </Col> : null
                            }
                        </Row>
                        <Row>
                            <Col>
                                <Switch>
                                    <Route path={"/authenticate"}>
                                        <Authenticate/>
                                    </Route>
                                    <Route path={"/register"}>
                                        <Register/>
                                    </Route>
                                    <Route path={"/registerSupport"}>
                                        <RegisterSupport/>
                                    </Route>
                                    <Route path={"/className"}>
                                        <ClasaDansList/>
                                    </Route>
                                    <Route path={"/programareClasa"}>
                                        <ProgramareClase/>
                                    </Route>
                                    <Route path={"/inscrieri"}>
                                        <InscrieriClase/>
                                    </Route>
                                    <Route path={"/mesaj"}>
                                        <Mesaj/>
                                    </Route>
                                    <Route path={"/calendar"}>
                                        <MyCalendar/>
                                    </Route>
                                </Switch>
                            </Col>
                        </Row>
                    </Container>
                </HashRouter>
            </div>
        );
    }
}

export default App;
