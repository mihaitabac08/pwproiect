import React from 'react';
import axios from 'axios';
import {Button, Form, Table} from "react-bootstrap";

const baseUrl = 'http://localhost:3001/api/v1';

class Mesaj extends React.Component {
    state = {
        authors: [],
        titlu: "",
        mesaj: "",
        raspuns: "",
        important: false,
        faq: false
    };

    componentDidMount() {
        if (localStorage.getItem('token') != null)
            this.loadmesaj();
    }

    render() {
        return (
            <div className="AuthorList" style={
                {
                    "padding": "25px",
                    "marginLeft": "25px",
                    "marginRight": "25px"
                }
            }>


                <Form onSubmit={this.submitHandler}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Titlu</Form.Label>
                        <Form.Control type="text"
                                      placeholder="Titlu"
                                      value={this.state.titlu}
                                      name="titlu"
                                      onChange={this.changeHandler}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Mesaj</Form.Label>
                        <Form.Control type="text"
                                      placeholder="Mesaj"
                                      value={this.state.mesaj}
                                      name="mesaj"
                                      onChange={this.changeHandler}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Raspuns</Form.Label>
                        <Form.Control type="text"
                                      placeholder="Raspuns"
                                      value={this.state.raspuns}
                                      name="raspuns"
                                      onChange={this.changeHandler}/>
                    </Form.Group>
                    <Form.Group controlId="important">
                        <Form.Label>Important</Form.Label>
                        <Form.Control type="checkbox"
                                      placeholder="Important"
                                      value={this.state.important}
                                      name="important"
                                      onChange={this.changeHandler}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>FAQ</Form.Label>
                        <Form.Control type="checkbox"
                                      placeholder="Faq"
                                      value={this.state.faq}
                                      name="faq"
                                      onChange={this.changeHandler}/>
                    </Form.Group>


                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>

                {
                    this.state.authors !== [] ?
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Id Mesaj</th>
                                <th>User Id</th>
                                <th>Titlu</th>
                                <th>Mesaj</th>
                                <th>Raspuns</th>
                                <th>Important</th>
                                <th>Faq</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.authors.map((author, index) =>
                                <tr key={index}>
                                    <td>{author._id}</td>
                                    <td>{author.idUser}</td>
                                    <td>{author.titlu}</td>
                                    <td>{author.mesaj}</td>
                                    <td>{author.raspuns}</td>
                                    <td>{author.important ? 'important': ''}</td>
                                    <td>{author.faq ? 'FAQ': ''}</td>
                                </tr>
                            )}
                            </tbody>
                        </Table>
                        : null
                }
            </div>
        )
    }

    loadmesaj = () => {
        console.log(localStorage.getItem('token'));
        axios.get(baseUrl + '/mesaj', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => this.setState({"authors": response.data}))
            .catch(error => console.error(error));
    };

    submitHandler = (event) => {
        event.preventDefault();

        const config = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        };

        const body = {
            titlu: this.state.titlu,
            mesaj: this.state.mesaj,
            raspuns: this.state.raspuns,
            important: this.state.important,
            faq: this.state.faq
        };

        axios.post(baseUrl + '/mesaj',
            body,
            config)
            .then(this.loadmesaj())
            .catch(error => console.error(error));
    };

    changeHandler = (event) => {
        event.preventDefault();
        let inputValue = event.target.value;
        const stateField = event.target.name;
        if(event.target.type === 'checkbox') {
            inputValue = event.target.checked;
        }
        this.setState({
            [stateField]: inputValue
        });
    };

}

export default Mesaj;
