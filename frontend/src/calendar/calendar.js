import React, {Component} from 'react';
//import { render } from 'react-dom';
import {Calendar, momentLocalizer} from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import axios from "axios";
//import ClasaDansList from "../ClasaDans/ClasaDansList";
const baseUrl = 'http://localhost:3001/api/v1';


const localizer = momentLocalizer(moment);

export class MyCalendar extends Component {
    constructor() {
        super();
        const now = new Date();
        const events = [
        ]
        this.state = {
            name: 'React',
            events
        };
    }

    componentDidMount() {
        if (localStorage.getItem('token') != null) {
            this.loadProgramareClasa();
        }
    }

    render() {
        return (
            <div>
                <p>
                    Calendar cursuri.
                </p>
                <div style={{height: '500pt'}}>
                    <Calendar
                        events={this.state.events}
                        startAccessor="start"
                        endAccessor="end"
                        defaultDate={moment().toDate()}
                        localizer={localizer}
                    />
                </div>
            </div>
        );
    }

    loadProgramareClasa = () => {
        console.log(localStorage.getItem('token'));
        axios.get(baseUrl + '/programareClasa', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => {
            this.setState({"authors": response.data, className: ""})
            let idx = 50;
            console.log(this.state.events);
            for (const programare of response.data) {
                let start = moment(programare.data + ' ' + programare.ora, 'DD.MM.YYYY HH:mm');
                let end = moment(programare.data + ' ' + programare.ora, 'DD.MM.YYYY HH:mm');
                this.state.events.push({
                    id: idx,
                    title: programare.clasaDans.className,
                    start: start.toDate(),
                    end: end.add(programare.durata, 'minutes').toDate(),
                })
                idx++;
                console.log(start);
            }
            console.log(this.state.events);

            this.setState({events: this.state.events})
        })
            .catch(error => console.error(error));
    };

}

export default MyCalendar;
