import React from 'react';
import axios from 'axios';
import {Button, Form, Table} from "react-bootstrap";

const baseUrl = 'http://localhost:3001/api/v1';

class InscrieriClase extends React.Component {
    state = {
        authors: [],
        programs: [],
        idProgramare: ""
    };

    componentDidMount() {
        if (localStorage.getItem('token') != null) {
            this.loadInscriereClasa();
            this.programs();
        }
    }

    render() {
        return (
            <div className="AuthorList" style={
                {
                    "padding": "25px",
                    "marginLeft": "25px",
                    "marginRight": "25px"
                }
            }>


                <Form onSubmit={this.submitHandler}>
                    {
                        this.state.programs !== [] ?
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Alegeti clasa</Form.Label>
                                <Form.Control as="select" custom
                                              name="idProgramare"
                                              value={this.state.idProgramare}
                                              onChange={this.changeHandler}>
                                    <option>---</option>
                                    {this.state.programs.map((clasa, index) =>
                                        <option key={index} value={clasa._id}>
                                            {clasa.clasaDans.className} -
                                            {clasa.data} -
                                            {clasa.ora}
                                        </option>
                                    )}
                                </Form.Control>
                            </Form.Group>
                            : null
                    }

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>

                {
                    this.state.authors !== [] ?
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Id Inscriere</th>
                                <th>Utilizator</th>
                                <th>Id Program</th>

                            </tr>
                            </thead>
                            <tbody>
                            {this.state.authors.map((author, index) =>
                                <tr key={index}>
                                    <td>{author._id}</td>
                                    <td>{author.idUser.username}</td>
                                    <td>{this.getclassname(author.idProgramare._id)} -
                                        {author.idProgramare.data} -
                                        {author.idProgramare.ora} -
                                        {author.idProgramare.durata}</td>

                                </tr>
                            )}
                            </tbody>
                        </Table>
                        : null
                }
            </div>
        )
    }

    programs = () => {
        console.log(localStorage.getItem('token'));
        axios.get(baseUrl + '/programareClasa', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => this.setState({programs: response.data }))
            .catch(error => console.error(error));
    };



    getclassname = (idclasa) => {
        const programe = this.state.programs;
        for (const program of programe) {
            if (program._id == idclasa)
                return program.clasaDans.className;
        }
        return '';
    };

    loadInscriereClasa = () => {
        console.log(localStorage.getItem('token'));
        axios.get(baseUrl + '/inscrieri', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => this.setState({"authors": response.data, idProgramare: ""}))
            .catch(error => console.error(error));
    };

    submitHandler = (event) => {
        event.preventDefault();

        const config = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        };

        const body = {
            idProgramare: this.state.idProgramare
        };

        axios.post(baseUrl + '/inscrieri',
            body,
            config)
            .then(this.loadInscriereClasa())
            .catch(error => console.error(error));
    };

    changeHandler = (event) => {
        event.preventDefault();
        const inputValue = event.target.value;
        const stateField = event.target.name;
        this.setState({
            [stateField]: inputValue
        });
    };

}

export default InscrieriClase;
