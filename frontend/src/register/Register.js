import React, {Component} from 'react';
import axios from 'axios';
import {Button, Card, Form} from "react-bootstrap";
import {toast} from 'react-toastify';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            username: '',
            password: ''
        }
    };

    registerHandler = (event) => {
        event.preventDefault();
        axios.post("http://localhost:3001/api/v1/Users/register", {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            username: this.state.username,
            password: this.state.password
        }).then(response => {
            toast("Utilizatorul a fost creat!");
            this.setState({
                firstName: '',
                lastName: '',
                email: '',
                username: '',
                password: ''
            });
        })
            .catch(error => {
                //toast(error.response.data.error);
                console.error(error)
            });
    };


    changeHandler = (event) => {
        event.preventDefault();
        const inputValue = event.target.value;
        const stateField = event.target.name;
        this.setState({
            [stateField]: inputValue
        });
        console.log(this.state);
    };

    render() {
        return (
            <div>
                <Card style={{
                    "padding": "10px",
                    "backgroundColor": "#f9f9f9"
                }}>
                    <Form className="AuthenticateForm" onSubmit={this.registerHandler}>

                        <Form.Group controlId="firstName">
                            <Form.Label>First Name:</Form.Label>
                            <Form.Control type="text"
                                          value={this.state.firstName}
                                          name="firstName"
                                          onChange={this.changeHandler}/>

                        </Form.Group>
                        <Form.Group controlId="lastName">
                            <Form.Label>Last Name:</Form.Label>
                            <Form.Control type="text"
                                          value={this.state.lastName}
                                          name="lastName"
                                          onChange={this.changeHandler}/>

                        </Form.Group>
                        <Form.Group controlId="email">
                            <Form.Label>Email:</Form.Label>
                            <Form.Control type="email"
                                          value={this.state.email}
                                          name="email"
                                          onChange={this.changeHandler}/>

                        </Form.Group>
                        <Form.Group controlId="username">
                            <Form.Label>Username:</Form.Label>
                            <Form.Control type="text"
                                          value={this.state.username}
                                          name="username"
                                          onChange={this.changeHandler}/>

                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>Password:</Form.Label>
                            <Form.Control type="password"
                                          value={this.state.password}
                                          name="password"
                                          onChange={this.changeHandler}/>

                        </Form.Group>

                        <Button variant="primary" type="submit">
                            Register
                        </Button>
                    </Form>
                </Card>
            </div>
        )
    }
}

export default Register;
