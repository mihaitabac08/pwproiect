import React from 'react';
import axios from 'axios';
import {Button, Form, Table} from "react-bootstrap";

const baseUrl = 'http://localhost:3001/api/v1';

class ProgramareClase extends React.Component {
    state = {
        authors: [],
        classes: [],
        durata: "",
        clasaDansId: "",
        ora: "",
        data: ""
    };

    componentDidMount() {
        if (localStorage.getItem('token') != null) {
            this.loadProgramareClasa();
            this.classes();
        }
    }

    render() {
        return (
            <div className="AuthorList" style={
                {
                    "padding": "25px",
                    "marginLeft": "25px",
                    "marginRight": "25px"
                }
            }>


                <Form onSubmit={this.submitHandler}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Durata clasa</Form.Label>
                        <Form.Control type="text"
                                      placeholder="Durata clasei"
                                      value={this.state.durata}
                                      name="durata"
                                      onChange={this.changeHandler}/>

                    </Form.Group>
                    {
                        this.state.classes !== [] ?
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Numele clasei</Form.Label>
                                <Form.Control as="select" custom
                                              name="clasaDansId"
                                              value={this.state.clasaDansId}
                                              onChange={this.changeHandler}>
                                    <option>---</option>
                                    {this.state.classes.map((clasa, index) =>
                                        <option key={index} value={clasa._id}>
                                            {clasa.className}
                                        </option>
                                    )}
                                </Form.Control>
                            </Form.Group>
                            : null
                    }
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Ora inceperi</Form.Label>
                        <Form.Control type="text"
                                      placeholder="Ora de incepere"
                                      value={this.state.ora}
                                      name="ora"
                                      onChange={this.changeHandler}/>
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Data</Form.Label>
                        <Form.Control type="text"
                                      placeholder="Data"
                                      value={this.state.data}
                                      name="data"
                                      onChange={this.changeHandler}/>
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>

                {
                    this.state.authors !== [] ?
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Durata</th>
                                <th>Nume clasa</th>
                                <th>Ora inceperii</th>
                                <th>Data</th>

                            </tr>
                            </thead>
                            <tbody>
                            {this.state.authors.map((author, index) =>
                                <tr key={index}>
                                    <td>{author._id}</td>
                                    <td>{author.durata}</td>
                                    <td>{author.clasaDans.className}</td>
                                    <td>{author.ora}</td>
                                    <td>{author.data}</td>

                                </tr>
                            )}
                            </tbody>
                        </Table>
                        : null
                }
            </div>
        )
    }

    classes = () => {
        console.log(localStorage.getItem('token'));
        axios.get(baseUrl + '/clasaDans', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
            }).then(response => this.setState({classes: response.data }))
            .catch(error => console.error(error));
    };

    loadProgramareClasa = () => {
        console.log(localStorage.getItem('token'));
        axios.get(baseUrl + '/programareClasa', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => this.setState({"authors": response.data, className: ""}))
            .catch(error => console.error(error));
    };

    submitHandler = (event) => {
        event.preventDefault();

        const config = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        };

        const body = {
            durata: this.state.durata,
            clasaDansId: this.state.clasaDansId,
            ora: this.state.ora,
            data: this.state.data
        };

        axios.post(baseUrl + '/programareClasa',
            body,
            config)
            .then(this.loadProgramareClasa())
            .catch(error => console.error(error));
    };

    changeHandler = (event) => {
        event.preventDefault();
        const inputValue = event.target.value;
        const stateField = event.target.name;
        this.setState({
            [stateField]: inputValue
        });
    };

}

export default ProgramareClase;
