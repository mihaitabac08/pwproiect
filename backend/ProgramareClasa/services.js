const {
    ProgramareClasa
} = require('../data');

const add = async (durata, clasaDansId, ora, data) => {
    // create new program obj
    // save it
    const newProgram = new ProgramareClasa(
        {
            durata: durata,
            clasaDans : clasaDansId,
            ora : ora,
            data: data
        }
    );
    await newProgram.save();

};

const getAll = async () => {
    // get all books
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    return await ProgramareClasa.find().populate('clasaDans');

};

const getById = async (id) => {
    // get book by id
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    return await ProgramareClasa.findById();
};

const getByClasaDansId = async (id) => {
    // get book by author id
    // modify output so author is made of 'author.firstName author.lastName'
    return await ProgramareClasa.find();
};

const updateById = async (id, durata, clasaDansId, ora, data) => {
    // update by id
    await ProgramareClasa.findByIdAndUpdate(id, {durata: durata, clasaDans: clasaDansId, ora: ora, data: data});
};

const deleteById = async (id) => {
    // delete by id
    await ProgramareClasa.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    getByClasaDansId,
    updateById,
    deleteById
}