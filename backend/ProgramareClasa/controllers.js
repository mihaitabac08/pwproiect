const express = require('express');

const ProgramareService = require('./services.js');
const {
    validateFields
} = require('../utils');
const {
    authorizeAndExtractToken
} = require('../security/Jwt');
const {
    ServerError
} = require('../errors');
const {
    authorizeRoles
} = require('../security/Roles');

const router = express.Router();

router.post('/', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        durata,
        clasaDansId,
        ora,
        data
    } = req.body;
    try {
        // do logic
        const fieldsToBeValidated = {
            durata: {
                value: durata,
                type: "alpha"
            },
            clasaDansId: {
                value: clasaDansId,
                type: "ascii"
            },
            ora: {
                value: ora,
                type: "genres-list"
            },
            data: {
                value: data,
                type: "alpha"
            }
        };

        await ProgramareService.add(durata, clasaDansId, ora, data);
        res.status(200).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        // pot sa primesc eroare si ca genul nu e bun, trebuie verificat mesajul erorii
        // HINT err.message
        if (err.message.includes('Not valid ora'))
            next (new ServerError("Invalid ora", 400));
        else
            next(err);
    }
});

router.get('/', authorizeAndExtractToken, authorizeRoles('admin', 'support', 'user'), async (req, res, next) => {
    try {
        // do logic
        const program = await ProgramareService.getAll();
        res.json(program);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/:id', authorizeAndExtractToken, authorizeRoles('admin', 'support', 'user'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        // do logic
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const program = await ProgramareService.getById(id);
        res.json(program);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/clasaDans/:id', authorizeAndExtractToken, authorizeRoles('admin', 'support', 'user'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        // do logic
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const program = await ProgramareService.getByClasaDansId(id);
        res.json(program);

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.put('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        durata,
        clasaDansId,
        ora,
        data
    } = req.body;
    try {
        // do logic
        const fieldsToBeValidated = {
            id: {
                value: id,
                type: "ascii"
            },
            durata: {
                value: durata,
                type: "alpha"
            },
            clasaDansId: {
                value: clasaDansId,
                type: "ascii"
            },
            ora: {
                value: ora,
                type: "genres-list"
            },
            data: {
                value: data,
                type: "alpha"
            }
        };

        await ProgramareService.updateById(id, durata, clasaDansId, ora, data);
        res.status(200).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js


        if (err.message.includes('Not valid ora'))
            next (new ServerError("Invalid ora", 400));
        else
            next(err);
    }
});

router.delete('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        // do logic
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        await ProgramareService.deleteById(id);
        res.status(200).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

module.exports = router;
