const express = require('express');

const mesajService = require('./services.js');
const {
    validateFields
} = require('../utils');
const {
    authorizeAndExtractToken
} = require('../security/Jwt');
const {
    ServerError
} = require('../errors');
const {
    authorizeRoles
} = require('../security/Roles');

const router = express.Router();

router.post('/', authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    const {
        titlu,
        mesaj,
        raspuns,
        important,
        faq
    } = req.body;
    try {
        // do logic
        const fieldsToBeValidated = {
            titlu: {
                value: titlu,
                type: "alpha"
            },
            mesaj: {
                value: mesaj,
                type: "alpha"
            },
            raspuns: {
                value: raspuns,
                type: "alpha"
            },
            important: {
                value: important,
                type: "alpha"
            },
            faq: {
                value: faq,
                type: "alpha"
            }
        };
        await mesajService.add(titlu, mesaj, raspuns, req.state.decoded.userId, important, faq);
        res.status(200).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        // pot sa primesc eroare si ca genul nu e bun, trebuie verificat mesajul erorii
        // HINT err.message
        if (err.message.includes('Not valid ora'))
            next (new ServerError("Invalid ora", 400));
        else
            next(err);
    }
});

router.get('/', authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    try {
        // do logic
        const program = await mesajService.getAll();
        res.json(program);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/:id', authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        // do logic
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const program = await mesajService.getById(id);
        res.json(program);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});


router.put('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        titlu,
        mesaj,
        raspuns,
        idUser,
        important,
        faq
    } = req.body;
    try {
        // do logic
        const fieldsToBeValidated = {
            titlu: {
                value: titlu,
                type: "alpha"
            },
            mesaj: {
                value: mesaj,
                type: "alpha"
            },
            raspuns: {
                value: raspuns,
                type: "alpha"
            },
            important: {
                value: important,
                type: "alpha"
            },
            faq: {
                value: faq,
                type: "alpha"
            },
            idUser: {
                value: idUser,
                type: "ascii"
            }
        };

        await mesajService.updateById(titlu, mesaj, raspuns, idUser, important, faq);
        res.status(200).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js


        if (err.message.includes('Not valid ora'))
            next (new ServerError("Invalid ora", 400));
        else
            next(err);
    }
});

router.delete('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        // do logic
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        await mesajService.deleteById(id);
        res.status(200).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

module.exports = router;