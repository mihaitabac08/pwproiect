const {
    Mesaj
} = require('../data');

const add = async (titlu, mesaj, raspuns, idUser, important, faq) => {
    // create new program obj
    // save it
    const newmesaj = new Mesaj(
        {
            titlu: titlu,
            mesaj: mesaj,
            raspuns: raspuns,
            idUser: idUser,
            important: important,
            faq: faq
        }
    );
    await newmesaj.save();

};

const getAll = async () => {
    // get all books
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    return await Mesaj.find();

};

const getById = async (id) => {
    // get book by id
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    return await Mesaj.findById();
};

const getByClasaDansId = async (id) => {
    // get book by author id
    // modify output so author is made of 'author.firstName author.lastName'
    return await Mesaj.find();
}

const updateById = async (titlu, mesaj, raspuns, idUser, important, faq) => {
    // update by id
    await Mesaj.findByIdAndUpdate(id, {titlu: titlu, mesaj: mesaj, raspuns: raspuns, idUser: idUser, important: important, faq: faq});
};

const deleteById = async (id) => {
    // delete by id
    await Mesaj.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    getByClasaDansId,
    updateById,
    deleteById
}