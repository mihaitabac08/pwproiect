const express = require('express');

const ClasaDansService = require('./services.js');

const {
    validateFields
} = require('../utils');
const {
    authorizeAndExtractToken
} = require('../security/Jwt');

const {
    authorizeRoles
} = require('../security/Roles');

const router = express.Router();

router.post('/', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        className,
    } = req.body;

    // validare de campuri
    try {

        const fieldsToBeValidated = {
            className: {
                value: className,
                type: 'alpha'
            }
        };

        validateFields(fieldsToBeValidated);

        await ClasaDansService.add(className);

        res.status(201).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/', authorizeAndExtractToken, authorizeRoles('admin', 'support', 'user'), async (req, res, next) => {
    try {

        const clasadans = await ClasaDansService.getAll();
        res.json(clasadans);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/:id', authorizeAndExtractToken, authorizeRoles('admin', 'support', 'user'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {

        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const clasadans = await ClasaDansService.getById(id);
        res.json(clasadans);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.put('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        className
    } = req.body;
    try {

        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'ascii'
            },
            className: {
                value: className,
                type: 'alpha'
            },
        };

        validateFields(fieldsToBeValidated);

        await ClasaDansService.updateById(id, className);
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.delete('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;

    try {

        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        // se poate modifica
        await ClasaDansService.deleteById(id);
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

module.exports = router;
