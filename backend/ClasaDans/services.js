const {
    ClasaDans,
} = require('../data');

const add = async (className) => {
    const clasa = new ClasaDans({
        className
    });
    await clasa.save();
};

const getAll = async () => {
    return await ClasaDans.find();
};

const getById = async (id) => {
    return await ClasaDans.findById(id);
};

const updateById = async (id, className) => {
    await ClasaDans.findByIdAndUpdate(id, { className });
};

const deleteById = async (id) => {
    await ClasaDans.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    updateById,
    deleteById
}