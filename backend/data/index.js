const mongoose = require('mongoose');

(async () => {
  try {
    await mongoose.connect(`mongodb://${process.env.MUSER}:${process.env.MPASSWORD}@${process.env.MHOST}:${process.env.MPORT}/${process.env.MDATABASE}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    );
  } catch (e) {
    console.trace(e);
  }
})();

const Users = require('./models/Users.js');
const ClasaDans = require('./models/ClasaDans.js');
const ProgramareClasa = require('./models/ProgramareClasa.js');
const Inscrieri = require('./models/Inscrieri.js');
const Mesaj = require('./models/Mesaj.js');

module.exports = {
  Users,
  ClasaDans,
  ProgramareClasa,
  Inscrieri,
  Mesaj
}
