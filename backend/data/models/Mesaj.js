
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MesajSchema = new Schema({
    titlu: {
        type: String,
        required: true
    },
    mesaj: {
        type: String,
        required: true
    },
    raspuns: {
        type: String,
        required: true
    },
    idUser: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    },
    important: {
        type: Boolean,
        required: false
    },
    faq: {
        type: Boolean,
        required: false
    }
}, { timestamps: true });

const MesajModel = mongoose.model('Mesaj', MesajSchema);
module.exports = MesajModel;
