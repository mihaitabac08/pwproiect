const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DansSchema = new Schema({
    className: {
        type: String,
        required: true
    }
}, { timestamps: true });

const DansModel = mongoose.model('ClasaDans', DansSchema);

module.exports = DansModel;
