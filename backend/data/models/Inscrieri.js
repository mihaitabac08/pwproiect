
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InscrieriSchema = new Schema({
    idProgramare: {
        type: Schema.Types.ObjectId,
        ref: 'ProgramareClasa',
        required: true
    },
    idUser: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    }

}, { timestamps: true });

const InscrieriModel = mongoose.model('Inscrieri', InscrieriSchema);
module.exports = InscrieriModel;
