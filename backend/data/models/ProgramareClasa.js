
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProgramSchema = new Schema({
    clasaDans: {
        type: Schema.Types.ObjectId,
        ref: 'ClasaDans',
        required: true
    },
    durata: {
        type: String,
        required: true
    },
    ora: {
        type: String
    },
    data: {
        type: String
    }

}, { timestamps: true });

const ProgramModel = mongoose.model('ProgramareClasa', ProgramSchema);
module.exports = ProgramModel;
