const {
    Inscrieri
} = require('../data');

const add = async (idProgramare, idUser) => {
    // create new program obj
    // save it
    console.log(idProgramare);
    console.log(idUser);
    const newInscrieri = new Inscrieri(
        {
            idProgramare: idProgramare,
            idUser: idUser
        }
    );
    await newInscrieri.save();

};

const getAll = async () => {
    // get all books
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    return await Inscrieri.find().populate('idProgramare idUser');
};

const getById = async (id) => {
    // get book by id
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    return await Inscrieri.findById();
};

const getByProgramareId = async (id) => {
    // get book by author id
    // modify output so author is made of 'author.firstName author.lastName'
    return await Inscrieri.find();
};

const updateById = async (id, idProgamare) => {
    // update by id
    await Inscrieri.findByIdAndUpdate(id, {idProgramare: idProgramare});
};

const deleteById = async (id) => {
    // delete by id
    await Inscrieri.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    getByProgramareId,
    updateById,
    deleteById
}