const express = require('express');

const InscrieriService = require('./services.js');

const {
    validateFields
} = require('../utils');
const {
    authorizeAndExtractToken
} = require('../security/Jwt');

const {
    authorizeRoles
} = require('../security/Roles');

const router = express.Router();

router.post('/', authorizeAndExtractToken, authorizeRoles('admin','user'), async (req, res, next) => {
    const {
        idProgramare: idProgramare
    } = req.body;
    const bearer = req.header('Authorization');
    console.log(req.state);
    // validare de campuri
    try {

        const fieldsToBeValidated = {
            idProgramare: {
                value: idProgramare,
                type: 'alpha'
            }
        };

        //validateFields(fieldsToBeValidated);

        await InscrieriService.add(idProgramare, req.state.decoded.userId);

        res.status(201).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/', authorizeAndExtractToken, authorizeRoles('admin', 'support', 'user'), async (req, res, next) => {
    try {

        const inscrieri = await InscrieriService.getAll();
        res.json(inscrieri);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/:id', authorizeAndExtractToken, authorizeRoles('admin', 'support', 'user'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {

        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const inscrieri = await InscrieriService.getById(id);
        res.json(inscrieri);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/programareClasa/:id', authorizeAndExtractToken, authorizeRoles('admin', 'support', 'user'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        // do logic
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const program = await InscrieriService.getByProgamareId(id);
        res.json(program);

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.put('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        idProgramare
    } = req.body;
    try {

        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'ascii'
            },
            idProgamare: {
                value: idProgramare,
                type: 'alpha'
            }
        };

        await InscrieriService.updateById(id, idProgramare);
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.delete('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;

    try {

        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        // se poate modifica
        await InscrieriService.deleteById(id);
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

module.exports = router;
