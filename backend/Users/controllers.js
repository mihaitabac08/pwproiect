const express = require('express');

const UsersService = require('./services.js');
const {
    validateFields
} = require('../utils');

const router = express.Router();

router.post('/register', async (req, res, next) => {
    const {
        firstName,
        lastName,
        email,
        username,
        password
    } = req.body;

    // validare de campuri
    try {
        const fieldsToBeValidated = {
            username: {
                value: username,
                type: 'alpha'
            },
            password: {
                value: password,
                type: 'ascii'
            }
        };
        //validateFields(fieldsToBeValidated);

        const user = {
            firstName,
            lastName,
            email,
            username,
            password
        };

        await UsersService.add(user);

        res.status(201).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.post('/registerSupport', async (req, res, next) => {
    const {
        username,
        password
    } = req.body;

    // validare de campuri
    try {
        const fieldsToBeValidated = {
            username: {
                value: username,
                type: 'alpha'
            },
            password: {
                value: password,
                type: 'ascii'
            }
        };
        //validateFields(fieldsToBeValidated);

        const user = {
            username,
            password,
            role: 'support'
        };

        await UsersService.add(user);

        res.status(201).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.post('/login', async (req, res, next) => {
    const {
        username,
        password
    } = req.body;

    try {
        const fieldsToBeValidated = {
            username: {
                value: username,
                type: 'alpha'
            },
            password: {
                value: password,
                type: 'ascii'
            }
        };

        validateFields(fieldsToBeValidated);

        const token = await UsersService.authenticate(username, password);

        res.status(200).json(token);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }
});

router.get('/activate/:code', async (req, res, next) => {
    const {
        code
    } = req.params;

    try {
        const response = await UsersService.activate(code);

        res.status(200).json(response);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
        next(err);
    }

});

module.exports = router;
