const cryptoRandomString = require('crypto-random-string');
const {
    Users
} = require('../data');

const {
    generateToken,
} = require('../security/Jwt');

const {
    ServerError
} = require('../errors');

const {
    hash,
    compare
} = require('../security/Password');



const add = async (user) => {
    const hashedPassword = await hash(user.password);

    let userDB;
    if(user.role === 'support') {
        userDB = new Users({
            username: user.username,
            password: hashedPassword,
            role: user.role
        });
    } else {
        let validationCode = cryptoRandomString({length: 10});
        const role = user.username === 'admin' ? 'admin' : 'user';

        userDB = new Users({
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            validationCode: validationCode,
            username: user.username,
            password: hashedPassword,
            role
        });

        const send = require('gmail-send')({
            user: 'calendardans@gmail.com',
            pass: 'calendardans1!',
            to:   user.email,
            subject: 'Proiect PW',
        });

        send({
            text:    'Activare cont: http://localhost:3001/api/v1/Users/activate/' + validationCode,
        }, (error, result, fullResult) => {
            if (error) console.error(error);
            console.log(result);
        });
    }

    await userDB.save();
};

const authenticate = async (username, password) => {

    const user = await Users.findOne({username});
    if (user === null) {
        throw new ServerError(`Utilizatorul inregistrat cu ${username} nu exista!`, 404);
    }

    if(user.validationCode) {
        throw new ServerError("Userul nu este activat!", 404);
    }

    if (await compare(password, user.password)) {
        return await generateToken({
            userId: user._id,
            userRole: user.role
        });
    }
    throw new ServerError("Combinatia de username si parola nu este buna!", 404);
};

const activate = async (code) => {

    const user = await Users.findOne({validationCode: code});
    if (user === null) {
        throw new ServerError(`Utilizatorul inregistrat cu ${username} nu exista!`, 404);
    }

    user.validationCode = null;
    user.save();

    return "Success";

};

module.exports = {
    add,
    authenticate,
    activate
};
