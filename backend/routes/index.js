const Router = require('express')();

const UsersController = require('../Users/controllers.js');
const ClasaDansController = require('../ClasaDans/Controllers.js');
const ProgramareClasaController = require('../ProgramareClasa/controllers.js');
const InscrieriController = require('../Inscrieri/controllers.js');
const mesajController = require('../mesaj/controllers.js');

Router.use('/users', UsersController);
Router.use('/clasaDans', ClasaDansController);
Router.use('/programareClasa', ProgramareClasaController);
Router.use('/inscrieri', InscrieriController);
Router.use('/mesaj', mesajController);

module.exports = Router;